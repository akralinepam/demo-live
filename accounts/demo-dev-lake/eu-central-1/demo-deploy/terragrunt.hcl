# Include all settings from the root variables.tfvars file
include {
  path = find_in_parent_folders()
}

terraform {
  # for local testing only
  source = "/Users/aleksei_kralin/dev/demo/demo-modules//demo-deploy"
}

locals {
  // map that hold common variables for all modules
  common_vars_file = find_in_parent_folders("common_vars.yaml")
  common_vars      = yamldecode(file(local.common_vars_file))

  // get the artifact version for current dataproduct
  json_cmdb         = jsondecode(file("${get_parent_terragrunt_dir()}/../../../demo-cmdb/cmdb.json"))["cmdb"]
  tfmodules_version = local.json_cmdb["demo-modules"]


  hooks = {
    "push_to_dev" = {
      filters = [
        {
          type                    = "EVENT"
          pattern                 = "PUSH"
          exclude_matched_pattern = false
        },
        {
          type                    = "HEAD_REF"
          pattern                 = "/dev"
          exclude_matched_pattern = false
        }
      ]
    }
  }
}

inputs = {
  codebuild_projects = {
    "adl-v2-deploy" = {
      description      = "terragrunt deployment"
      build_timeout    = 20
      source_type      = "BITBUCKET"
      source_location  = "https://bitbucket.org/alconpc/demo-cmdb.git"
      source_buildspec = "arn:aws:s3:::adl-artifacts-repo/cmdb-buildspec.yml"
      secondary_sources = {
        adl_live = {
          type              = "BITBUCKET"
          location          = "https://bitbucket.org/alconpc/demo-tf-live.git"
          source_identifier = "adl_tf_live"
        }
      }
      artifact_path            = ""
      secondary_artifacts      = {}
      environment_compute_type = "BUILD_GENERAL1_MEDIUM"
      environment_image        = "aws/codebuild/standard:3.0"
      environment_type         = "LINUX_CONTAINER"
      environment_variables = {
        ARTIFACT_BUCKET                   = local.common_vars["artifact_bucket"]
        ADL_ENV                           = local.common_vars["default_environment"]
        ADL_TF_MODULES_VERSION            = local.tfmodules_version
        TF_PLUGIN_CACHE_DIR               = "$HOME/.terraform.d/plugin-cache"
      }
      enable_cloudwatch_trigger = false
      schedule_expression       = "cron(0 0 ? * * 1970)"
      event_pattern             = ""
      hooks                     = local.hooks
    }
  }
}
