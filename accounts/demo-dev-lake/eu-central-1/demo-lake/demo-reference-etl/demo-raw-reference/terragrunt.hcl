# Include all settings from the root variables.tfvars file
include {
  path = find_in_parent_folders()
}

terraform {
  // if you want to use exact version of module set version explicetly
  source = "git::https://bitbucket.org/akralinepam/demo-modules.git//demo-dataproduct?ref=${local.tfmodules_version}"
  //source = "/Users/aleksei_kralin/dev/demo/demo-modules//demo-dataproduct"

  before_hook "before_hook_1" {
    commands = get_terraform_commands_that_need_vars()
    execute = [
      "aws",
      "s3",
      "cp",
      "s3://${local.artifact_bucket}/${local.git_repository}/releases/${local.artifacts_version}/${local.current_folder}.tf",
    "override.tf"]
    run_on_error = false
  }
}

locals {
  // map that hold common variables for all modules
  common_vars_file = find_in_parent_folders("common_vars.yaml")
  common_vars      = yamldecode(file(local.common_vars_file))

  // get git repository name for current dataproduct
  git_repository = basename(dirname(get_terragrunt_dir()))
  current_folder = basename(get_terragrunt_dir())

  // get the artifact version for current dataproduct
  json_cmdb         = jsondecode(file("${get_parent_terragrunt_dir()}/../../../demo-cmdb/cmdb.json"))["cmdb"]
  artifacts_version = local.json_cmdb[local.git_repository]
  tfmodules_version = local.json_cmdb["demo-modules"]

  // global artifact bucket
  artifact_bucket = yamldecode(file(local.common_vars_file))["artifact_bucket"]
}

inputs = {
  artifacts_version          = local.artifacts_version
  secondary_artifact_version = local.json_cmdb
  local_tags = {
    Layer              = local.current_folder
    DataProduct        = local.git_repository
    DataProductVersion = local.artifacts_version
    TFModulesVersion   = local.tfmodules_version
  }
}
