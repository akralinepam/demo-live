remote_state {
  backend = "s3"

  config = {
    encrypt        = true
    bucket         = "tfstate-${get_env("DEMO_ENV", "dev")}-${get_aws_account_id()}"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "tfstate-lock"

    skip_metadata_api_check     = true
    skip_region_validation      = true
    skip_credentials_validation = true
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite"
  contents  = <<EOF
provider "aws" {
  version     = "= ${get_env("TF_AWS_PROVIDER_VERSION", local.common_vars.tf_aws_provider_version)}"
  region      = var.aws_region
}
EOF
}

locals {
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

inputs = {
    aws_region  = "eu-central-1"
    environment = get_env("DEMO_ENV", "dev")
    tfstate_v2_s3_bucket = "tfstate-${get_env("DEMO_ENV", "dev")}-${get_aws_account_id()}"
    tfstate_lake_key_prefix = "us-east-1/adl-lake"
    artifact_bucket = yamldecode(file(find_in_parent_folders("common_vars.yaml")))["artifact_bucket"]
    iam_path        = "/demo/"
    tags = {
        TerraformManaged = true
        Environment      = get_env("ADL_ENV", "dev")
    }
}