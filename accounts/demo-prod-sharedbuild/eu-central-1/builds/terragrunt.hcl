# Include all settings from the root variables.tfvars file
include {
  path = find_in_parent_folders()
}

terraform {
  # for local testing only
  source = "/Users/aleksei_kralin/dev/demo/demo-modules//aws-codebuild"
}

locals {
  hooks = {
    "push_to_develop_release" = {
      filters = [
        {
          type                    = "EVENT"
          pattern                 = "PUSH"
          exclude_matched_pattern = false
        },
        {
          type                    = "HEAD_REF"
          pattern                 = "/develop|/release/"
          exclude_matched_pattern = false
        }
      ]
    },
    "pr_feature_to_develop" = {
      filters = [
        {
          type                    = "EVENT"
          pattern                 = "PULL_REQUEST_CREATED,  PULL_REQUEST_UPDATED"
          exclude_matched_pattern = false
        },
        {
          type                    = "BASE_REF"
          pattern                 = "/develop"
          exclude_matched_pattern = false
        },
        {
          type                    = "HEAD_REF"
          pattern                 = "/feature/"
          exclude_matched_pattern = false
        }
      ]
    },
    "pr_develop_or_hotfix_to_release" = {
      filters = [
        {
          type                    = "EVENT"
          pattern                 = "PULL_REQUEST_CREATED,  PULL_REQUEST_UPDATED"
          exclude_matched_pattern = false
        },
        {
          type                    = "BASE_REF"
          pattern                 = "/release/"
          exclude_matched_pattern = false
        },
        {
          type                    = "HEAD_REF"
          pattern                 = "/develop|/hotfix/"
          exclude_matched_pattern = false
        }
      ]
    }
  }

  common_vars_file     = find_in_parent_folders("common_vars.yaml")
  common_vars          = yamldecode(file(local.common_vars_file))
  json_cmdb            = jsondecode(file("${get_parent_terragrunt_dir()}/../../../demo-cmdb/cmdb.json"))["cmdb"]
#   tfmodules_version    = local.json_cmdb["demo-tf-modules"]
  artifact_bucket      = local.common_vars["artifact_bucket"]

}

inputs = {
  artifact_bucket_prefix = "demo-artifacts"
  shared_bucket_arns = [
      "arn:aws:s3:::aws-glue-etl-artifacts"
    ]

  codebuild_projects = {
    "demo-reference-etl" = {
      description      = "demo-reference-etl"
      build_timeout    = 5
      source_type      = "BITBUCKET"
      source_location  = "https://bitbucket.org/akralinepam/demo-reference-etl.git"
      source_buildspec = "config/buildspec.yml"
      secondary_sources = {
        cmdb = {
          type              = "BITBUCKET"
          location          = "https://bitbucket.org/akralinepam/demo-cmdb.git"
          source_identifier = "cmdb"
        }
      }
      artifact_path            = "releases/"
      secondary_artifacts      = {}
      environment_compute_type = "BUILD_GENERAL1_LARGE"
      environment_image        = "aws/codebuild/standard:3.0"
      environment_type         = "LINUX_CONTAINER"
      environment_variables = {
      }
      hooks = local.hooks
    },
    "demo-modules" = {
      description      = "demo-modules"
      build_timeout    = 5
      source_type      = "BITBUCKET"
      source_location  = "https://bitbucket.org/akralinepam/demo-modules.git"
      source_buildspec = "config/buildspec.yml"
      secondary_sources = {
        cmdb = {
          type              = "BITBUCKET"
          location          = "https://bitbucket.org/akralinepam/demo-cmdb.git"
          source_identifier = "cmdb"
        }
      }
      artifact_path            = "releases/"
      secondary_artifacts      = {}
      environment_compute_type = "BUILD_GENERAL1_LARGE"
      environment_image        = "aws/codebuild/standard:3.0"
      environment_type         = "LINUX_CONTAINER"
      environment_variables = {
      }
      hooks = local.hooks
    },
  }
}
